#include "Pizzeria.h"

namespace PizzeriaLibrary {

int Pizza::ComputePrice() const
{
    int price=0;
    unsigned int numIngredients = this->NumIngredients();
    for (unsigned int i=0; i<numIngredients; i++){
        price += _ingredients[i].Price;
    }
    return price;
};

void Order::InitializeOrder(const int &numPizzas)
{
    if (!numPizzas){
        throw runtime_error("Empty Order");
    }
}

const Pizza & Order::GetPizza(const unsigned int &position) const
{
    if (position <= _pizzas.size()){
        return _pizzas[position-1];
    } else {
        throw runtime_error("Invalid position");
    }
}

int Order::ComputeTotal() const
{
    int total=0;
    unsigned int numPizzas = _pizzas.size();
    for (unsigned int i=0; i < numPizzas; i++){
        total+=_pizzas[i].ComputePrice();
    }
    return total;
}

void Pizzeria::AddIngredient(const string &name, const string &description, const int &price)
{
    for (list<Ingredient>::iterator it = _kitchen.begin(); it!=_kitchen.end(); it++){
       if (it->Name == name){
           throw runtime_error("Ingredient already inserted");
        }
    }
    _kitchen.push_back(Ingredient(name, description, price));
}

const Ingredient &Pizzeria::FindIngredient(const string &name) const
{
    for (list<Ingredient>::iterator it = _kitchen.begin(); it!=_kitchen.end(); it++){
       if (it->Name == name){
           return *it;
       }
    }
    throw runtime_error("Ingredient not found");
}

void Pizzeria::AddPizza(const string &name, const vector<string> &ingredients)
{
    for (list<Pizza>::iterator it = _menu.begin(); it!=_menu.end(); it++){
       if (it->Name == name){
           throw runtime_error("Pizza already inserted");
       }
    }
    _menu.push_back(Pizza(name));
    for (unsigned int i=0; i<ingredients.size(); i++){
        Pizza & pizzaAdd = _menu.back();
        pizzaAdd.AddIngredient(FindIngredient(ingredients[i]));}
}

const Pizza &Pizzeria::FindPizza(const string &name) const
{
    for (list<Pizza>::iterator it = _menu.begin(); it!=_menu.end(); it++){
       if (it->Name == name){
           return *it;
       }
    }
    throw runtime_error("Pizza not found");
}

int Pizzeria::CreateOrder(const vector<string> &pizzas)
{ 
    if (!pizzas.size()){throw runtime_error("Empty order");}
    _orders.push_back(Order());
    for (unsigned int i=0; i< pizzas.size(); i++){
        _orders.back().AddPizza(this->FindPizza(pizzas[i]));}
        return _orders.size() + 999;
    }

const Order &Pizzeria::FindOrder(const int &numOrder) const
{
    int numOr =_orders.size();
    if (numOrder-1000 < numOr){
        return _orders[numOrder-1000];
    } else{
        throw runtime_error("Order not found");}
}

string Pizzeria::GetReceipt(const int &numOrder) const
{
    Order orderReceipt = this->FindOrder(numOrder);
    string receipt = "";
    for (unsigned int i=0; i<orderReceipt._pizzas.size(); i++){
        receipt+= ("- " + orderReceipt._pizzas[i].Name + ", " + to_string(orderReceipt._pizzas[i].ComputePrice()) + " euro" + "\n");
    }
    receipt += "  TOTAL: " + to_string(orderReceipt.ComputeTotal()) + " euro" + "\n";
    return receipt;
}

string Pizzeria::ListIngredients() const
{
    string kitchen = "";
    _kitchen.sort();
    for (list<Ingredient>::iterator it = _kitchen.begin(); it!=_kitchen.end(); it++){
        kitchen += it->Name + " - '" + it->Description + "': " + to_string(it->Price) + " euro" + "\n";
    }
    return kitchen;
}

string Pizzeria::Menu() const
{
    string menu = "";
    for (list<Pizza>::iterator it = _menu.begin(); it!=_menu.end(); it++){
        menu += it->Name + " (" + to_string(it->NumIngredients()) + " ingredients): " + to_string(it->ComputePrice()) + " euro" + "\n";
    }
    return menu;
}

}
