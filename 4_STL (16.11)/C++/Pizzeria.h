#ifndef PIZZERIA_H
#define PIZZERIA_H

#include <iostream>
#include <vector>
#include <list>

using namespace std;

namespace PizzeriaLibrary {

  class Ingredient {
    public:
      string Name;
      int Price;
      string Description;

      Ingredient(const string &name, const string& description, const int &price){
          Name = name;
          Description = description;
          Price = price;
      }
      bool operator < (const Ingredient &ingredient){
          return Name < ingredient.Name;
      }
  };

  class Pizza {
    public:
      string Name;
      mutable vector<Ingredient> _ingredients;

      Pizza(const string &name){
          Name = name;
      }
      bool operator < (const Pizza &pizza){
          return Name < pizza.Name;
      }

      void AddIngredient(const Ingredient& ingredient){_ingredients.push_back(ingredient);};
      int NumIngredients() const {return _ingredients.size();};
      int ComputePrice() const;
  };

  class Order {
    public:
      vector<Pizza> _pizzas;

      void InitializeOrder(const int &numPizzas);
      void AddPizza(const Pizza& pizza){ _pizzas.push_back(pizza); };
      const Pizza& GetPizza(const unsigned int& position) const;
      int NumPizzas() const {return _pizzas.size();};
      int ComputeTotal() const;
  };

  class Pizzeria {
    public:
     mutable list<Pizza> _menu;
     mutable list<Ingredient> _kitchen;
     mutable vector <Order> _orders;
      void AddIngredient(const string& name,
                         const string& description,
                         const int& price);
      const Ingredient& FindIngredient(const string& name)const;
      void AddPizza(const string& name,
                    const vector<string>& ingredients);
      const Pizza& FindPizza(const string& name)const;
      int CreateOrder(const vector<string>& pizzas);
      const Order& FindOrder(const int& numOrder) const;
      string GetReceipt(const int& numOrder) const;
      string ListIngredients() const;
      string Menu() const;
  };
};

#endif // PIZZERIA_H
