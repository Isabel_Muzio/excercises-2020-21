#ifndef __TEST_PIZZA_H
#define __TEST_PIZZA_H

#include <gtest/gtest.h>
#include <gmock/gmock.h>
#include <gmock/gmock-matchers.h>

#include "Pizzeria.h"

using namespace PizzeriaLibrary;

using namespace testing;

namespace PizzeriaTesting {

  TEST(TestPizzeria, TestIngredient)
  {
    Pizzeria pizzeria;

    try
    {
      pizzeria.AddIngredient("Tomato", "Red berry of the plant Solanum lycopersicum", 2);
      pizzeria.AddIngredient("Mozzarella", "Traditionally southern Italian cheese", 3);
    }
    catch (const exception& exception)
    {
      FAIL();
    }

    try
    {
      pizzeria.AddIngredient("Mozzarella", "Traditionally southern Italian cheese", 3);
      FAIL();
    }
    catch (const exception& exception)
    {
      EXPECT_THAT(std::string(exception.what()), Eq("Ingredient already inserted"));
    }

    try
    {
      const Ingredient& ingredient = pizzeria.FindIngredient("Mozzarella");
      EXPECT_EQ(ingredient.Name, "Mozzarella");
      EXPECT_EQ(ingredient.Description, "Traditionally southern Italian cheese");
      EXPECT_EQ(ingredient.Price, 3);
    }
    catch (const exception& exception)
    {
      FAIL();
    }

    try
    {
      const Ingredient& ingredient = pizzeria.FindIngredient("Eggplant");
      FAIL();
    }
    catch (const exception& exception)
    {
      EXPECT_THAT(std::string(exception.what()), Eq("Ingredient not found"));
    }

    try
    {
      EXPECT_EQ(pizzeria.ListIngredients(), "Mozzarella - 'Traditionally southern Italian cheese': 3 euro\nTomato - 'Red berry of the plant Solanum lycopersicum': 2 euro\n");
    }
    catch (const exception& exception)
    {
      FAIL();
    }
  }

  TEST(TestPizzeria, TestPizza)
  {
    Pizzeria pizzeria;

    try
    {
      pizzeria.AddIngredient("Tomato", "Red berry of the plant Solanum lycopersicum", 2);
      pizzeria.AddIngredient("Mozzarella", "Traditionally southern Italian cheese", 3);

      pizzeria.AddPizza("Margherita", vector<string> { "Tomato", "Mozzarella" });
      pizzeria.AddPizza("Marinara", vector<string> { "Tomato" });
    }
    catch (const exception& exception)
    {
      FAIL();
    }

    try
    {
      pizzeria.AddPizza("Margherita", vector<string> { "Tomato", "Mozzarella" });
      FAIL();
    }
    catch (const exception& exception)
    {
      EXPECT_THAT(std::string(exception.what()), Eq("Pizza already inserted"));
    }

    try
    {
      const Pizza& pizza = pizzeria.FindPizza("Margherita");
      EXPECT_EQ(pizza.Name, "Margherita");
      EXPECT_EQ(pizza.NumIngredients(), 2);
      EXPECT_EQ(pizza.ComputePrice(), 5);
    }
    catch (const exception& exception)
    {
      FAIL();
    }

    try
    {
      const Pizza& pizza = pizzeria.FindPizza("Prosciutto");
      FAIL();
    }
    catch (const exception& exception)
    {
      EXPECT_THAT(std::string(exception.what()), Eq("Pizza not found"));
    }

    try
    {
      EXPECT_EQ(pizzeria.Menu(), "Margherita (2 ingredients): 5 euro\nMarinara (1 ingredients): 2 euro\n");
    }
    catch (const exception& exception)
    {
      FAIL();
    }
  }

  TEST(TestPizzeria, TestOrder)
  {
    Pizzeria pizzeria;

    try
    {
      pizzeria.AddIngredient("Tomato", "Red berry of the plant Solanum lycopersicum", 2);
      pizzeria.AddIngredient("Mozzarella", "Traditionally southern Italian cheese", 3);

      pizzeria.AddPizza("Margherita", vector<string> { "Tomato", "Mozzarella" });
      pizzeria.AddPizza("Marinara", vector<string> { "Tomato" });

      EXPECT_EQ(pizzeria.CreateOrder(vector<string> { "Margherita", "Marinara" }), 1000);
    }
    catch (const exception& exception)
    {
      FAIL();
    }

    try
    {
      pizzeria.CreateOrder(vector<string>());
      FAIL();
    }
    catch (const exception& exception)
    {
      EXPECT_THAT(std::string(exception.what()), Eq("Empty order"));
    }

    try
    {
      const Order& order = pizzeria.FindOrder(1000);
      EXPECT_EQ(order.NumPizzas(), 2);
      EXPECT_EQ(order.ComputeTotal(), 7);
    }
    catch (const exception& exception)
    {
      FAIL();
    }

    try
    {
      const Order& order = pizzeria.FindOrder(1001);
      FAIL();
    }
    catch (const exception& exception)
    {
      EXPECT_THAT(std::string(exception.what()), Eq("Order not found"));
    }


    try
    {
      pizzeria.GetReceipt(1001);
      FAIL();
    }
    catch (const exception& exception)
    {
      EXPECT_THAT(std::string(exception.what()), Eq("Order not found"));
    }

    try
    {
      EXPECT_EQ(pizzeria.GetReceipt(1000), "- Margherita, 5 euro\n- Marinara, 2 euro\n  TOTAL: 7 euro\n");
    }
    catch (const exception& exception)
    {
      FAIL();
    }
  }
}

#endif // __TEST_PIZZA_H
