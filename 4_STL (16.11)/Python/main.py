class Ingredient:
    def __init__(self, name: str, description: str, price: int):
        self.Name = name
        self.Description = description
        self.Price = price


class Pizza:
    def __init__(self, name: str):
        self.Name = name
        self.ingredients = []

    def addIngredient(self, ingredient: Ingredient):
        if ingredient in self.ingredients:
            ingredient.Description = "Reinforcement"
            self.ingredients.append(ingredient)
        else:
            self.ingredients.append(ingredient)

    def numIngredients(self) -> int:
        return len(self.ingredients)

    def computePrice(self) -> int:
        price = 0
        for ingredient in self.ingredients:
            price += ingredient.Price
        return price


class Order:
    def __init__(self):
        self.pizzas = []
        self.numOrder: int = 0

    def getPizza(self, position: int) -> Pizza:
        if position <= len(self.pizzas):
            return self.pizzas[position - 1]
        else:
            raise ValueError("Position passed is wrong")

    def initializeOrder(self, numOrder: int):
        self.numOrder = numOrder + 1000

    def addPizza(self, pizza: Pizza):
        self.pizzas.append(pizza)

    def numPizzas(self) -> int:
        return len(self.pizzas)

    def computeTotal(self) -> int:
        total = 0
        for i in range(0, len(self.pizzas)):
            total += self.pizzas[i].computePrice()
        return total


class Pizzeria:
    def __init__(self):
        self.kitchen = []
        self.pizzamenu = []
        self.orders = []

    def addIngredient(self, name: str, description: str, price: int):
        numIngredients = len(self.kitchen)
        for i in range(0, numIngredients):
            if name is self.kitchen[i].Name:
                raise ValueError("Ingredient already inserted")
        self.kitchen.append(Ingredient(name, description, price))

    def findIngredient(self, name: str) -> Ingredient:
        for ingredient in self.kitchen:
            if name is ingredient.Name:
                return ingredient
        raise ValueError("Ingredient not found")

    def addPizza(self, name: str, ingredients: []):
        numPizzas = len(self.pizzamenu)
        for i in range(0, numPizzas):
            if name is self.pizzamenu[i].Name:
                raise ValueError("Pizza already inserted")
            elif name < self.pizzamenu[i].Name:
                pizzaAdd = Pizza(name)
                for ingredient in ingredients:
                    pizzaAdd.addIngredient(self.findIngredient(ingredient))
                self.pizzamenu.insert(i, pizzaAdd)
                break
        pizzaAdd = Pizza(name)
        for ingredient in ingredients:
            pizzaAdd.addIngredient(self.findIngredient(ingredient))
        self.pizzamenu.append(pizzaAdd)

    def findPizza(self, name: str) -> Pizza:
        for pizza in self.pizzamenu:
            if name is pizza.Name:
                return pizza
        found = False
        if not found:
            raise ValueError("Pizza not found")

    def createOrder(self, pizzas: []) -> int:
        if len(pizzas) is 0:
            raise ValueError("Empty order")
        orderAdd = Order()
        orderAdd.initializeOrder(len(self.orders))
        for i in range(0, len(pizzas)):
            orderAdd.addPizza(self.findPizza(pizzas[i]))
        self.orders.append(orderAdd)
        return orderAdd.numOrder


    def findOrder(self, numOrder: int) -> Order:
        if numOrder - 1000 < len(self.orders):
            return self.orders[numOrder-1000]
        else:
            raise ValueError("Order not found")

    def getReceipt(self, numOrder: int) -> str:
        orderReceipt = self.findOrder(numOrder)
        receipt = ""
        for pizza in orderReceipt.pizzas:
            receipt += "- " + pizza.Name + ", " + str(pizza.computePrice()) + " euro" + "\n"
        receipt += "  TOTAL: " + str(orderReceipt.computeTotal()) + " euro" + "\n"
        return receipt

    def listIngredients(self) -> str:
        kitchen = ""
        self.kitchen.sort(key=lambda x: x.Name)
        for ingredient in self.kitchen:
            kitchen += ingredient.Name + " - '" + ingredient.Description + "': " + str(ingredient.Price) + " euro" + "\n"
        return kitchen

    def menu(self) -> str:
        menu = ""
        for pizza in self.pizzamenu:
            menu += pizza.Name + " (" + str(pizza.numIngredients()) + " ingredients): " + str(pizza.computePrice()) + " euro" + "\n"
        return menu
