# include "viaMichelin.h"

#include <iostream>
#include <fstream>
#include <sstream>

namespace ViaMichelinLibrary {
  int RoutePlanner::BusAverageSpeed = 50;

  void BusStation::Load()
  {
     _numberBuses = 0;
     _buses.clear();

     ifstream file;
     file.open(_busFilePath.c_str());
     if(file.fail())
         throw runtime_error("Something goes wrong");

     try {
         string line;

     //get number Buses
     getline(file, line); //skip comment line
     getline(file, line);

     istringstream convertN;
     convertN.str(line);
     convertN>>_numberBuses;

     //get Buses
     getline(file, line); //skip comment line
     _buses.resize(_numberBuses);
     for (int b=0; b <_numberBuses; b++){
         getline(file, line);
         istringstream bConverter;
         bConverter.str(line);
         bConverter >> _buses[b].Id >> _buses[b].FuelCost;
        }

     file.close();

     } catch(exception) {
         _numberBuses = 0;
         _buses.clear();
         throw runtime_error("Something goes wrong");
     }


  }

  const Bus &BusStation::GetBus(const int &idBus) const
  {
      if (idBus > _numberBuses)
          throw runtime_error("Bus " + to_string(idBus) + " does not exists");
      return _buses[idBus - 1];

  }

  void MapData::Load()
  {
      _numberRoutes = 0;
      _numberStreets = 0;
      _numberBusStops = 0;
      _busStops.clear();
      _streets.clear();
      _routes.clear();
      _streetFrom.clear();
      _streetTo.clear();
      _routeStreets.clear();

      ifstream file;
      file.open(_mapFilePath.c_str());
      if(file.fail())
          throw runtime_error("Something goes wrong");

      try {

      string line;

      //get number BusStops
      getline(file, line); //skip comment line
      getline(file, line);

      istringstream BSconverter;
      BSconverter.str(line);
      BSconverter>>_numberBusStops;

      //get BusStops
      getline(file, line); //skip comment line

      _busStops.resize(_numberBusStops);
      for (int b=0; b <_numberBusStops; b++){
          getline(file, line);
          istringstream converter;
          converter.str(line);
          converter >> _busStops[b].Id >> _busStops[b].Name >> _busStops[b].Latitude >> _busStops[b].Longitude;

         }

      //get number Streets
      getline(file, line); //skip comment line
      getline(file, line);

      istringstream numStrConverter;
      numStrConverter.str(line);
      numStrConverter>>_numberStreets;

      //get Streets
      getline(file, line); //skip comment line

      _streets.resize(_numberStreets);
      _streetFrom.resize(_numberStreets);
      _streetTo.resize(_numberStreets);
      for (int s=0; s <_numberStreets; s++){
          getline(file, line);
          istringstream converter;
          converter.str(line);
          converter >> _streets[s].Id >> _streetFrom[s] >> _streetTo[s] >> _streets[s].TravelTime;

         }
      //get number Routes
      getline(file, line); //skip comment line
      getline(file, line);

      istringstream numRoutesConverter;
      numRoutesConverter.str(line);
      numRoutesConverter>>_numberRoutes;

      //get Routes
      getline(file, line); //skip comment line

      _routes.resize(_numberRoutes);
      _routeStreets.resize(_numberRoutes);
      for (int r=0; r <_numberRoutes; r++){

          getline(file, line);
          istringstream converter;
          converter.str(line);
          converter >> _routes[r].Id >> _routes[r].NumberStreets;
          _routeStreets[r].resize(_routes[r].NumberStreets);

          for (int s = 0; s < _routes[r].NumberStreets; s++)
            converter >> _routeStreets[r][s];

         }
      file.close();

      } catch(exception) {
          _numberRoutes = 0;
          _numberStreets = 0;
          _numberBusStops = 0;
          _busStops.clear();
          _streets.clear();
          _routes.clear();
          _streetFrom.clear();
          _streetTo.clear();
          _routeStreets.clear();
          throw runtime_error("Something goes wrong");
      }
  }

  const Street &MapData::GetRouteStreet(const int &idRoute, const int &streetPosition) const
  {
      if (idRoute > _numberRoutes)
          throw runtime_error("Route " + to_string(idRoute) + " does not exists");

      const vector<int>& streets = _routeStreets[idRoute -1];
      if (streetPosition >= (int)streets.size()){
          throw runtime_error("Street at position " + to_string(streetPosition)+ " does not exists");
      }
      const int& idStreet = streets[streetPosition];
      return GetStreet(idStreet);
  }

  const Route &MapData::GetRoute(const int &idRoute) const
  {
      if (idRoute > _numberRoutes)
          throw runtime_error("Route " + to_string(idRoute) + " does not exists");
      return _routes[idRoute - 1];
  }

  const Street &MapData::GetStreet(const int &idStreet) const
  {
      if (idStreet > _numberStreets)
          throw runtime_error("Street " + to_string(idStreet) + " does not exists");
      return _streets[idStreet - 1];
  }

  const BusStop &MapData::GetStreetFrom(const int &idStreet) const
  {
      if (idStreet > _numberStreets)
          throw runtime_error("Street " + to_string(idStreet) + " does not exists");

      const int& idBusStop = _streetFrom[idStreet -1];

      return GetBusStop(idBusStop);
  }

  const BusStop &MapData::GetStreetTo(const int &idStreet) const
  {
      if (idStreet > _numberStreets)
          throw runtime_error("Street " + to_string(idStreet) + " does not exists");

      const int& idBusStop = _streetTo[idStreet -1];

      return GetBusStop(idBusStop);
  }

  const BusStop &MapData::GetBusStop(const int &idBusStop) const
  {
      if (idBusStop > _numberBusStops)
          throw runtime_error("BusStop " + to_string(idBusStop) + " does not exists");
      return _busStops[idBusStop - 1];
  }

  int RoutePlanner::ComputeRouteTravelTime(const int &idRoute) const
  {
      int totalTravelTime = 0;

      const Route& route = _mapData.GetRoute(idRoute);
      const int& numberStreets = route.NumberStreets;

      for (int s=0; s<numberStreets; s++){
          const Street& street = _mapData.GetRouteStreet(idRoute, s);
          totalTravelTime += street.TravelTime;
      }

      return totalTravelTime;
  }

  int RoutePlanner::ComputeRouteCost(const int &idBus, const int &idRoute) const
  {
      const Bus& bus = _busStation.GetBus(idBus);
      int routeCost = (double(this->ComputeRouteTravelTime(idRoute))/3600) * bus.FuelCost * BusAverageSpeed;
      return routeCost;
  }

  string MapViewer::ViewRoute(const int &idRoute) const
  {
      const Route& route = _mapData.GetRoute(idRoute);

      ostringstream routeView;
      routeView << route.Id << ": ";
      for (int s=0; s < route.NumberStreets; s++){
          const Street& street = _mapData.GetRouteStreet(idRoute, s);
          const BusStop& from = _mapData.GetStreetFrom(street.Id);
          routeView << from.Name << " -> ";

          if (s==route.NumberStreets -1){
              const BusStop& to = _mapData.GetStreetTo(street.Id);
              routeView << to.Name;
          }
      }

      return routeView.str();
  }

  string MapViewer::ViewStreet(const int &idStreet) const
  {
      const BusStop& from = _mapData.GetStreetFrom(idStreet);
      const BusStop& to = _mapData.GetStreetTo(idStreet);

      return to_string(idStreet) + + ": " + from.Name + " -> " + to.Name;
  }

  string MapViewer::ViewBusStop(const int &idBusStop) const
  {
      const BusStop& busStop = _mapData.GetBusStop(idBusStop);

      return busStop.Name + " (" + to_string((double)busStop.Latitude/10000) + ", " +
              to_string((double)busStop.Longitude/10000)+ ")";
  }
}
