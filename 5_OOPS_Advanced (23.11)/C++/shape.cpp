#include "shape.h"
#include <math.h>

namespace ShapeLibrary {

  double Ellipse::Perimeter() const
  {
      return M_PI*(_a + _b);
  }

  Triangle::Triangle(const Point& p1,
                     const Point& p2,
                     const Point& p3)
  {

      points.push_back(p1);
      points.push_back(p2);
      points.push_back(p3);
  }

  double Triangle::Perimeter() const
  {
    double perimeter = 0;
    for (unsigned int i=0; i<3; i++){
        perimeter += (points[(i+1)%3] - points[i]).ComputeNorm2();
    }
    return perimeter;
  }

  Quadrilateral::Quadrilateral(const Point& p1,
                               const Point& p2,
                               const Point& p3,
                               const Point& p4)
  {
      points.push_back(p1);
      points.push_back(p2);
      points.push_back(p3);
      points.push_back(p4);

  }

  double Quadrilateral::Perimeter() const
  {
      double perimeter = 0.0;
      for (unsigned int i=0; i<4; i++){
          perimeter += (points[(i+1)%4] - points[i]).ComputeNorm2();
      }
      return perimeter;
  }

  Point Point::operator+(const Point& point) const
  {
            Point tmp = Point(this->X + point.X, this->Y + point.Y);
            return tmp;
        }

  Point Point::operator-(const Point& point) const
  {
            Point tmp = Point(this->X - point.X, this->Y - point.Y);
            return tmp;
        }

  Point&Point::operator-=(const Point& point)
  {
            X -= point.X;
            Y -= point.Y;
            return *this;
        }

  Point&Point::operator+=(const Point& point)
  {
            X += point.X;
            Y += point.Y;
            return *this;
  }

  double Circle::Perimeter() const
  {
      return M_PI*2*(_a);
  }

  double Rectangle::Perimeter() const
  {
      return 2*(_base + _height);
  }

  double Square::Perimeter() const
  {
      return 4*_edge;
  }



}
