#ifndef SHAPE_H
#define SHAPE_H

#include <iostream>
#include <vector>
#include <math.h>
using namespace std;

namespace ShapeLibrary {

  class Point {
    public:
      double X;
      double Y;
      Point() { X = 0.0; Y = 0.0;}
      Point(const double& x,
            const double& y): X(x), Y(y){};
      Point(const Point& point) {
          X = point.X;
          Y = point.Y;
      }

      double ComputeNorm2() const { return sqrt((this->X)*(this->X) + (this->Y)*(this->Y)); };

      Point operator+(const Point& point) const;
      Point operator-(const Point& point) const;
      Point& operator-=(const Point& point);
      Point& operator+=(const Point& point);
      friend ostream& operator<<(ostream& stream, const Point& point)
      {
        stream << "Point: x = " <<to_string(int(point.X))<<" y = "<< to_string(int(point.Y))<<"\n";
        return stream;
      }
  };

  class IPolygon {
    public:
      virtual double Perimeter() const = 0;
      virtual void AddVertex(const Point& point) = 0;
      friend inline bool operator< (const IPolygon& lhs, const IPolygon& rhs){
          return lhs.Perimeter()<rhs.Perimeter()?true: false;
      }
      friend inline bool operator> (const IPolygon& lhs, const IPolygon& rhs){
          return lhs.Perimeter()>rhs.Perimeter()?true: false; }
      friend inline bool operator<=(const IPolygon& lhs, const IPolygon& rhs){
          return lhs.Perimeter()<=rhs.Perimeter()?true: false;
      }
      friend inline bool operator>=(const IPolygon& lhs, const IPolygon& rhs){
          return lhs.Perimeter()>=rhs.Perimeter()?true: false;
      }

  };

  class Ellipse : public IPolygon
  {
    protected:
      Point _center;
      double _a;
      double _b;

    public:
      Ellipse() { }
      Ellipse(const Point& center,
              const double& a,
              const double& b): _center(center), _a(a), _b(b){};
      virtual ~Ellipse() { }
      void AddVertex(const Point& point) { _center = point;}
      double Perimeter() const;
  };

  class Circle : public Ellipse
  {
    public:
      Circle() { }
      Circle(const Point& center,
             const double& radius) {
          _center = center;
          _a = radius;
          _b = radius;
      }
      virtual ~Circle() { }
      double Perimeter() const;
  };


  class Triangle : public IPolygon
  {
    protected:
      vector<Point> points;

    public:
      Triangle() { }
      Triangle(const Point& p1,
               const Point& p2,
               const Point& p3);

      void AddVertex(const Point& point) {
          points.push_back(point);
      }
      double Perimeter() const;
  };


  class TriangleEquilateral : public Triangle
  {
    private:
      double _edge;
    public:
      TriangleEquilateral(const Point& p1,
                          const double& edge): _edge(edge){points.push_back(p1);};
      double Perimeter() const{
          return 3*_edge;
      };
  };

  class Quadrilateral : public IPolygon
  {
    protected:
      vector<Point> points;

    public:
      Quadrilateral() { }
      Quadrilateral(const Point& p1,
                    const Point& p2,
                    const Point& p3,
                    const Point& p4);
      virtual ~Quadrilateral() { }
      void AddVertex(const Point& p) {
          points.push_back(p);
      }

      double Perimeter() const;
  };

  class Rectangle : public Quadrilateral
  {
  private:
      double _base;
      double _height;
    public:
      Rectangle() { }
      Rectangle(const Point& p1,
                const Point& p2,
                const Point& p3,
                const Point& p4) {
          Quadrilateral(p1, p2, p3, p4);
      }
      Rectangle(const Point& p1,
                const double& base,
                const double& height): _base(base), _height(height){ points.push_back(p1);};
      virtual ~Rectangle() { }
      double Perimeter() const;
  };

  class Square: public Rectangle
  {
  private:
      double _edge;
  public:
      Square() {}
      Square(const Point& p1,
             const Point& p2,
             const Point& p3,
             const Point& p4){
          Quadrilateral(p1, p2, p3, p4);
      }
      Square(const Point& p1,
             const double& edge): _edge(edge){ points.push_back(p1);};
      virtual ~Square() { }
      double Perimeter() const;
  };
}

#endif // SHAPE_H
