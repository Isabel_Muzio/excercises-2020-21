#ifndef SHAPE_H
#define SHAPE_H

#include <iostream>

using namespace std;

namespace ShapeLibrary {

  class Point {
    public:
      double _x;
      double _y;
      Point(const double& x = 0.0,
            const double& y = 0.0);
      Point(const Point& point) {
          _x = point._x;
          _y = point._y;
      }
  };

  class IPolygon {
    public:
      virtual double Area() const = 0;
  };

  class Ellipse : public IPolygon
  {
      protected:
      Point _centre;
      int _a;
      int _b;
    public:
      Ellipse(const Point& center = Point(),
              const int& a = 0,
              const int& b = 0);

      double Area() const;
  };

  class Circle : public IPolygon, private Ellipse
  {
  private:
      int _radius;
    public:
      Circle(const Point& center,
             const int& radius);

      double Area() const;
  };


  class Triangle : public IPolygon
  {
  protected:
      Point _p1;
      Point _p2;
      Point _p3;
    public:
      Triangle(const Point& p1 = Point(),
               const Point& p2 = Point(),
               const Point& p3 = Point());

      double Area() const;
  };


  class TriangleEquilateral : public IPolygon, private Triangle
  {
  private:
      Point _p1;
      int _edge;
    public:
      TriangleEquilateral(const Point& p1,
                          const int& edge);

      double Area() const;
  };

  class Quadrilateral : public IPolygon
  {
      protected:
      Point _p1;
      Point _p2;
      Point _p3;
      Point _p4;
    public:
      Quadrilateral(const Point& p1 = Point(),
                    const Point& p2 = Point(),
                    const Point& p3 = Point(),
                    const Point& p4 = Point());

      double Area() const;
  };


  class Parallelogram : public IPolygon, protected Quadrilateral
  {
      protected:
      Point _p1;
      Point _p2;
      Point _p4;
    public:
      Parallelogram(const Point& p1 = Point(),
                    const Point& p2 = Point(),
                    const Point& p4 = Point());

      double Area() const;
  };

  class Rectangle : public IPolygon, protected Parallelogram
  {
  protected:
      Point _p1;
      int _base;
      int _height;
    public:
      Rectangle(const Point& p1 = Point(),
                const int& base = 0,
                const int& height = 0);

      double Area() const;
  };

  class Square: public IPolygon, private Rectangle
  {
  private:
      int _edge;
    public:
      Square(const Point& p1,
             const int& edge);

      double Area() const;
  };
}

#endif // SHAPE_H
