#include "shape.h"
#include <math.h>

namespace ShapeLibrary {

Point::Point(const double &x, const double &y)
{
    _x = x;
    _y = y;
}

Ellipse::Ellipse(const Point &center, const int &a, const int &b)
{
              _centre = Point(center);
              _a = a;
              _b = b;
}


double Ellipse::Area() const
{
    return M_PI*_a*_b;
};

Circle::Circle(const Point &center, const int &radius)
{
    _centre = Point(center);
    _radius = radius;
}

double Circle::Area() const
{
    return M_PI*_radius*_radius;
};


Triangle::Triangle(const Point &p1, const Point &p2, const Point &p3)
{
    _p1 = Point(p1);
    _p2 = Point(p2);
    _p3 = Point(p3);
}

double Triangle::Area() const
{

    //we compute the area through the definition of cross product between two vectors, in which we consider the two edges as such
    double cross;
    cross = abs((_p1._x-_p2._x)*(_p3._y-_p2._y)-((_p3._x-_p2._x)*((_p1._y-_p2._y))));
    return (1.0/2.0)*cross;
}

TriangleEquilateral::TriangleEquilateral(const Point &p1, const int &edge)
{
    _p1 = Point(p1);
    _edge = edge;
}

double TriangleEquilateral::Area() const
{
    return (_edge/2)*_edge*(sin(M_PI/3));
}


Quadrilateral::Quadrilateral(const Point &p1, const Point &p2, const Point &p3, const Point &p4)
{
    _p1 = Point(p1);
    _p2 = Point(p2);
    _p3 = Point(p3);
    _p4 = Point(p4);
}

double Quadrilateral::Area() const
{
    double a1, a2;
    Triangle t1, t2;
    t1 = Triangle(_p1, _p2, _p3);
    t2 = Triangle (_p1, _p3,_p4);
    a1 = t1.Area();
    a2 = t2.Area();
    return a1+a2;
}


Parallelogram::Parallelogram(const Point &p1, const Point &p2, const Point &p4)
{
    _p1 = Point(p1);
    _p2 = Point(p2);
    _p4 = Point(p4);
}

double Parallelogram::Area() const
{
    //we compute the area through the definition of cross product between two vectors, in which we consider the two edges as such
    double cross;
    cross = abs((_p1._x-_p2._x)*(_p4._y-_p1._y)-((_p4._x-_p1._x)*((_p1._y-_p2._y))));
    return cross;
}

Rectangle::Rectangle(const Point &p1, const int &base, const int &height)
{
    _p1 = Point(p1);
    _base = base;
    _height = height;

}

double Rectangle::Area() const
{
    return _base*_height;
}

Square::Square(const Point &p1, const int &edge)
{
    _p1 =Point(p1);
    _edge = edge;
}

double Square::Area() const
{
    return _edge*_edge;
}

}
