import math as math


class Point:
    def __init__(self, x: float = None, y: float = None):
        self.x = x
        self.y = y


class IPolygon:
    def area(self) -> float:
        pass


class Ellipse(IPolygon):
    def __init__(self, center: Point, a: int, b: int):
        self._centre = center
        self._a = a
        self._b = b

    def area(self) -> float:
        return math.pi*self._a*self._b


class Circle(Ellipse):
    def __init__(self, center: Point, radius: int):
        self.__centre = center
        self.__radius = radius


    def area(self) -> float:
        return math.pi*self.__radius*self.__radius


class Triangle(IPolygon):
    def __init__(self, p1: Point, p2: Point, p3: Point):
        self._p1 = p1
        self._p2 = p2
        self._p3 = p3

    def area(self) -> float:
        cross = abs((self._p1.x-self._p2.x)*(self._p3.y-self._p2.y)-(self._p3.x-self._p2.x)*(self._p1.y-self._p2.y))
        return (1.0/2.0)*cross


class TriangleEquilateral(Triangle):
    def __init__(self, p1: Point, edge: int):
        self._p1 = p1
        self._edge = edge

    def area(self) -> float:
        return (1.0/2.0)*math.sin(math.pi/3)*self._edge*self._edge


class Quadrilateral(IPolygon):
    def __init__(self, p1: Point, p2: Point, p3: Point, p4: Point):
        self._p1 = p1
        self._p2 = p2
        self._p3 = p3
        self._p4 = p4

    def area(self) -> float:
        a1 = Triangle(self._p1, self._p2, self._p3).area()
        a2 = Triangle(self._p1, self._p4, self._p3).area()
        return a1+a2


class Parallelogram(Quadrilateral):
    def __init__(self, p1: Point, p2: Point, p4: Point):
        self._p1 = p1
        self._p2 = p2
        self._p4 = p4

    def area(self) -> float:
        cross = abs((self._p1.x - self._p2.x) * (self._p4.y - self._p2.y) - (self._p4.x - self._p2.x) * (
                    self._p1.y - self._p2.y))
        return cross


class Rectangle(Parallelogram):
    def __init__(self, p1: Point, base: int, height: int):
        self._p1 = p1
        self._base = base
        self._height = height

    def area(self) -> float:
        return self._base*self._height


class Square(Rectangle):
    def __init__(self, p1: Point, edge: int):
        self.__p1 = p1
        self.__edge = edge

    def area(self) -> float:
        return self.__edge*self.__edge
