#include "Intersector2D1D.h"
#include "Eigen"
#include "iostream"

// ***************************************************************************
Intersector2D1D::Intersector2D1D()
{

}
Intersector2D1D::~Intersector2D1D()
{
    delete(planeNormalPointer);
    delete(planeTranslationPointer);
    delete(lineOriginPointer);
    delete(lineTangentPointer);
}
// ***************************************************************************
void Intersector2D1D::SetPlane(const Vector3d& planeNormal, const double& planeTranslation)
{
    double * tmpT = new double;
    *tmpT = planeTranslation;
    planeTranslationPointer = tmpT;
    Vector3d * tmpN = new Vector3d;
    *tmpN = planeNormal/planeNormal.norm();
    planeNormalPointer = tmpN;
}
// ***************************************************************************
void Intersector2D1D::SetLine(const Vector3d& lineOrigin, const Vector3d& lineTangent)
{
    Vector3d * tmpO = new Vector3d;
    *tmpO = lineOrigin;
    lineOriginPointer = tmpO;
    Vector3d * tmpT = new Vector3d;
    *tmpT = lineTangent/lineTangent.norm();
    lineTangentPointer = tmpT;
}
// ***************************************************************************
bool Intersector2D1D::ComputeIntersection()
{
    Vector3d normal = *planeNormalPointer;
    Vector3d tangent = *lineTangentPointer;
    Vector3d origin = *lineOriginPointer;
    double d = *planeTranslationPointer;
    double dot = normal.dot(tangent);
    double b = normal.dot(origin);
    if (dot<=toleranceIntersection){
        if (abs(b - d)<= toleranceParallelism){
            intersectionType = Coplanar;
        } else {
           intersectionType = NoInteresection;
        }
        return false;
    }
    intersectionParametricCoordinate = (d - b)/dot;
    intersectionType = PointIntersection;
    return true;
}
